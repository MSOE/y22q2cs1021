package slm5;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Slm5 extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("slm5.fxml"));
        stage.setTitle("Login");
        stage.setScene(new Scene(root, 300, 250));
        stage.show();
    }
}
