package wk1;

import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        char a = 'A';
        ArrayList<Integer> data = new ArrayList<>();
        String word = "brick";
        System.out.println(lessThanTen(word));
        double average = getMean(data);
        System.out.println(isLowercaseLetter(a));
        System.out.println(a);
        int i = a;
        System.out.println(i);
    }

    private static boolean lessThanTen(String word) {
        return (word!=null && word.length()<10);
    }

    private static double getMean(ArrayList<Integer> data) {
        int sum = 0;
        for(int value : data) {
            sum += value;
        }
        return (data.size()>0) ? (double)sum/data.size() : Double.NaN;
    }

    private static boolean isLowercaseLetter(char a) {
        return (a>='a' && a<='z');
    }

    public static void main2(String[] args) {
        byte bite = 22;
        char character = 'a';
        short shortInteger = 3333;
        int integer = 44444;
        long longInteger = 5555555555L;
        float floatingPoint = 6.67F;
        double doublePrecision = Math.PI;
        longInteger = Math.round(3.9);
        integer = (int) (doublePrecision + 3.8);
        integer = (int)3.0/8;
    }
}
