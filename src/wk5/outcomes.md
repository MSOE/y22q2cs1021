# GUI Components
 - [x] Be able to use SceneBuilder with IntelliJ
 - [x] Use SceneBuilder to create an FXML file describing a GUI layout

# FX Markup Language
 - [x] Describe the differences between creating JavaFX applications using imperative programming and using
       FXML with declarative style programming
 - [x] Implement controller classes and make appropriate modifications to FXML files to provide functionality
       to UI controls such as `Button` and `TextField` classes
 - [x] Use `Alert` and `TextInputDialog` classes to interact with the user

# Exception Handling
 - [x] Explain how exception handling increases the robustness of software
 - [x] Define exception; explain the concepts of `catch` and `throw` as they relate to exceptions
 - [x] Explain why `Error` exceptions should not be caught in application code
 - [x] For a given program and input condition, determine the control flow of a try-catch block
 - [x] Implement a method that catches an exception thrown by a class in the Java Standard Library
 - [x] Implement a method that uses a try-catch block and a repetition statement (i.e., loop) to validate user input
 - [x] Distinguish between checked and unchecked exceptions
 - [x] Explain the concept of exception propagation; explain the outcome if an exception is thrown and not caught by
       the program
 - [x] Explain the consequences of not handling a checked exception within a method where the checked exception occurs

 - [x] Inspect a call stack trace displayed by an unhandled exception to determine what caused the exception to be thrown
 - [x] Use multiple catch blocks to customize the way different types of exceptions are handled
 - [x] Use the throws clause to postpone when a checked exception is handled
 - [x] For a given program and input condition, determine the control flow of a try-catch block with and without the finally clause






