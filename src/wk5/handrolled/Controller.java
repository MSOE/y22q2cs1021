package wk5.handrolled;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private TextField input;

    @FXML
    private Label response;

    @FXML
    private void handleEnter(ActionEvent actionEvent) {
        response.setText("enter depressed");
    }

    @FXML
    private void handleClear(ActionEvent actionEvent) {
        response.setText("");
    }
}
