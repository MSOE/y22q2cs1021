package wk5.scenebuilder;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.Optional;

public class Controller {
    @FXML
    private TextField input;
    @FXML
    private Label response;

    @FXML
    private void handleEnter(ActionEvent actionEvent) {
        String text = input.getText();
        int age = Integer.MIN_VALUE;
        do {
            try {
                age = Integer.parseInt(text);
                int useless = 5 / age;
                boolean isTeenager = age > 12 && age < 20;
                response.setText("You are " + (isTeenager ? "" : "not ") + "a teenager");
            } catch (IllegalArgumentException e) {
                TextInputDialog dialog = new TextInputDialog("2");
                dialog.setHeaderText("Really? I need an integer. Try again");
                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()) {
                    text = result.get();
                }
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
            } finally {
                System.out.println("Always runs");
            }
        } while(age==Integer.MIN_VALUE);
    }

    @FXML
    private void handleClear(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to clear?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            response.setText("");
        }
    }
}
