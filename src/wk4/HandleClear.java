package wk4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.effect.BoxBlur;

public class HandleClear implements EventHandler<ActionEvent> {
    @Override
    public void handle(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        button.setEffect(new BoxBlur());
    }
}
