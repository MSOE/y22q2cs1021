# Announcements
 * Lunch with a professor
    - Noon today
    - Noon next Tuesday
    - Noon next Thursday
 * Be sure to watch SLM 4 and submit notes
 * Lab 5 is available
 * SML 5 is available, but may make sense to wait until next Tuesday to view
 
# GUI Components
 - [x] List at least three types of objects that can be contained in a `Parent` object
 - [x] Design and implement a graphical user interface (GUI) programs using the `Label`
       and `Button` classes from the JavaFX package
 - [x] Differentiate between layout panes such as: `GridPane`, `FlowPane`, `HBox`, and `VBox`
 - [x] Use the layout panes listed above to arrange components on a scene

# Event-Driven Programming
 - [x] Derive from the `Application` class and create a simple GUI application
 - [x] Create a `FlowPane` and add multiple components to the pane
 - [x] Explain what it means to "register an event handler"
 - [x] Write a method reference representing an instance method of a particular object
 - [x] Use a method reference to register an event handler to a `Button` or `TextField`
 - [x] Explain the role of the `ActionEvent` passed to the method reference
 - [x] Determine the source of the event that called an event handler
 - [x] Describe the event-delegation model and explain the role of the event source 
       object and the event listener
 - [x] List two classes whose instances are the source of events (e.g., `Button`)
 - [x] List two classes whose instances are event objects (e.g., `ActionEvent`)
 - [x] Implement the `EventHandler` interface as an inner class
 - [x] Implement an `EventHandler` as a lambda expression