package wk4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Scanner;

public class Teenager extends Application implements EventHandler<ActionEvent> {

    private Label response;
    private TextField input;
    public static final int HEIGHT = 200;
    public static final int WIDTH = 300;

    @Override
    public void start(Stage stage) {
        response = new Label();
        input = new TextField();
        Button clear = new Button("Clear");
        Pane pane = new FlowPane();
        pane.getChildren().add(new Label("Please enter your age: "));
        pane.getChildren().add(input);
        pane.getChildren().add(response);
        pane.getChildren().add(clear);
        input.setOnAction(this::handleInput);
        clear.setOnAction(this);

        stage.setScene(new Scene(pane, WIDTH, HEIGHT));
        stage.show();
    }

    private void handleInput(ActionEvent event) {
        String text = input.getText();
        Scanner parser = new Scanner(text);
        if(parser.hasNextInt()) {
            int age = parser.nextInt();
            boolean isTeenager = age>12 && age<20;
            response.setText("You are " + (isTeenager ? "" : "not ") + "a teenager");
        } else {
            response.setText("Please enter an integer");
        }
    }

    public void handle(ActionEvent actionEvent) {
        if(actionEvent.getSource()==input) {
            handleInput(actionEvent);
        } else {
            response.setText("");
        }
    }
}
