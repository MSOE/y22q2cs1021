package wk3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Driver extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Label message = new Label("Hello World!");
        message.setBackground(new Background(new BackgroundFill(Color.AQUAMARINE,
                new CornerRadii(50), null)));
        message.setFont(new Font(40));
        Scene scene = new Scene(message);

        stage.setTitle("Title bar");
        stage.setScene(scene);
        stage.show();
    }
}
