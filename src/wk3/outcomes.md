# Announcements
 * Lab 2 due tonight... any questions?
 * Lab 3 posted... any questions?
 * Conflict with 2:00-2:30pm office hour, shifted to 3:30-4pm
 * Quiz on Wednesday covering through today's lecture
 * Bring laptop to class on Thursday... in-class activity

# Inheritance

 - [x] Create an abstract method; describe the purpose of abstract methods
 - [x] Describe the differences between an abstract class and an interface
 - [x] Explain the role of the `Object` class
 - [x] Explain how automatic type promotion works with object references
 - [x] Override the `equals()` and `toString()` methods for user defined classes
 - [x] Explain the relationship between a reference type and the type of the object to which the reference points
 - [x] Explain the concept of polymorphism/dynamic binding
 - [x] Read code that uses inheritance and polymorphism and determine its output on execution
 - [x] Identify legal and illegal assignments of references to objects based on the reference type and object type
 - [x] Explain what it means for a class to implement an interface
 - [x] Use the `protected` modifier in defining an inheritance hierarchy
 - [x] Describe when to use an `abstract` class
 - [x] Explain why Java prohibits multiple inheritance