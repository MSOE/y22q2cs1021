# Exam Review Questions

``` Java
public class ExamQuestionDriver {
    public static void main(String[] args) {
        int[] ints = {1, 1, 2, 3, 4, 5};
        int index = 0;
        do {
            try {
                System.out.println(12.0 / (ints[++index] - 1));
            } catch(___ e) {
                if(ints[0] > 0) {
                    System.out.println("Oops");
                } else if(ints[0] == 0) {
                    System.out.println("Cannot divide by 0");
                }
            }
        } while(index < ints.length);
    }
}
```

1. What type of Exception must be caught in the catch statement for the program to not throw an exception to the console?
    1. NumberFormatException
    2. ArithmeticException
    3. IndexOutOfBoundsException
    4. IllegalArgumentException
2. What will be printed in the console once the program finishes running? (All output from all printlns)

3. Write two ways to add an action to the Javafx button when clicked.

``` Java
Button button = new Button();

button.setOnAction(actionEvent -> { ... });
button.setOnAction(new EventHandler<ActionEvent> {
    public void handle(ActionEvent event) {
        ...
    }});
```

``` xml
<button onAction="#handle" />
```

4. Describe the difference between imperative and declarative programing.
5. What is a stack trace and how can you use it to debug your code?
6. Write a method that takes in a valid filename which creates and copies the information
   in the passed in file to a new file in the source directory.
7. Describe the functions of the abstract classes that are used for file I/O?
8. T/F If a try block catches an exception, the finally block won’t run.
9. In what cases do you not catch an exception?
10. T / F: It is best practice to catch the most specific type of exception possible







