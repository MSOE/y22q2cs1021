package wk6;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class FileStuff {

    public static void main2(String[] args) throws IOException {
        Path path = Path.of("data/javaFresh.txt");
        OutputStream outputStream = Files.newOutputStream(path);
        PrintWriter out = new PrintWriter(outputStream);
        out.println("Hello from Java");
        out.close();
        Scanner in = new Scanner(Path.of("data/ideFresh.txt"));
        while(in.hasNext()) {
            System.out.println(in.next());
        }

    }


    public static void main(String[] args) {
        writeBytes(Path.of("data/bytesFromJava.bin"), false);
    }

    public static void writeBytes(Path path, boolean append) {
        //try (FileOutputStream out = new FileOutputStream(path.toFile(), append)) {
        OpenOption openOption = append ? StandardOpenOption.APPEND : StandardOpenOption.CREATE;
        try (OutputStream out = Files.newOutputStream(path, openOption)) {
            out.write('a');
            out.write('d');
            out.write(1000000);
            out.write(-1000000);
            byte[] byteArray = { 5, 20, 32, 'C', 'T' };
            out.write(byteArray);
            System.out.println("Data written");
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void readBytes(Path path) {
        //try (FileInputStream in = new FileInputStream(path.toFile())) {
        try (InputStream in = Files.newInputStream(path)) {
            int i = in.read();
            int j = in.read();
            int k = in.read();
            int l = in.read();
            int m = in.read();
            byte[] byteArray = new byte[5];
            int count = in.read(byteArray);
            if(5!=count) {
                System.err.println("Couldn't find five bytes in the file.  Only read " + count);
            }
            System.out.println("Data read");
            System.out.println("" + i + " " + j + " " + k + " " + l + " " + m);
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void writePrimitives(Path path, boolean append) {
        OpenOption openOption = append ? StandardOpenOption.APPEND : StandardOpenOption.CREATE;
        try (OutputStream out = Files.newOutputStream(path, openOption);
             DataOutputStream dOut = new DataOutputStream(out)) {
            dOut.writeInt(43);
            dOut.writeBoolean(false);
            dOut.writeDouble(Math.PI);
            System.out.println("Data written");
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
            System.err.println(e.getMessage());
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void readPrimitives(Path path) {
        try (InputStream in = Files.newInputStream(path);
            DataInputStream dIn = new DataInputStream(in)) {
                int i = dIn.readInt();
                boolean j = dIn.readBoolean();
                double k = dIn.readDouble();
                System.out.println("Data read");
                System.out.println("" + i + " " + j + " " + k);
            }
        catch(FileNotFoundException e) {
                System.err.println("File not found");
                System.err.println(e.getMessage());
            }
        catch(IOException e) {
                System.err.println(e.getMessage());
            }
        }

    public static void writeText(String filename) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(filename)) {
            out.println("This is so much fun!");
            System.out.println("Data written");
        }
    }

    public static void readText(Path path) throws IOException {
        try (Scanner in = new Scanner(path)) {
            System.out.println("Reading data:");
            while(in.hasNextLine()) {
                System.out.println(in.nextLine());
            }
        }
    }

}
