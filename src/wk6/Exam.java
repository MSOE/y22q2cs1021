package wk6;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Exam {
    public static void main(String[] args) {
        try {
            copyFile("data/javaFresh.txt");
        } catch (IOException e) {
            System.err.println("Couldn't copy your problematic file.");
        }
    }

    private static void copyFile(String filename) throws IOException {
        String wholeFile = Files.readString(Path.of(filename));
        String newFilename = filename.substring(0, filename.length()-4) + "Copy.txt";
        Files.writeString(Path.of(newFilename), wholeFile);
    }

    private static void copy(String filename) throws IOException {
        String wholeFile = "";
        try (Scanner in = new Scanner(Files.newInputStream(Path.of(filename)))) {
            while(in.hasNextLine()) {
                wholeFile += in.nextLine() + "\n";
            }
        }
        String newFilename = filename.substring(0, filename.length()-4) + "Copy.txt";
        try (PrintWriter out = new PrintWriter(Files.newOutputStream(Path.of(newFilename)))) {
            out.print(wholeFile);
        }
    }
}
