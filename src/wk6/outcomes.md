# Files and Directories
 - [ ] Create a Java `Path` object and associate it with a file on disk
 - [ ] Determine if a file exists on disk
 - [ ] Determine if a Java `Path` object is associated with a file or a directory
 - [ ] Convert between `Path` and `File` objects
 - [ ] Use the `Files` class to obtain input and output streams

# Input and Output (I/O)
 - [ ] Associate a low-level input (i.e., `FileInputStream`) or output (i.e., `FileOutputStream`) stream with a `File` object
 - [ ] Describe how low-level file I/O works (i.e., reading or writing of byte data, importance of the sequence of data)
 - [ ] Describe how high-level (`DataOutputStream` and `DataInputStream`) file I/O works
       (i.e., methods for reading and writing of primitive data types, association with low-level stream,
       the importance of the sequence of data)
 - [ ] Explain why it is important to close a stream when file operations are complete
 - [ ] Use the try-with-resources construct to ensure resources are closed appropriately
 - [ ] Explain what the `PrintWriter` class is used for
 - [ ] Read text data from a file using `Scanner` objects
 - [ ] Use the `Files` class to read/write text files
 - [ ] Explain the difference between binary and text files
 - [ ] Describe some of the important exceptions that the java file IO classes generate
 - [ ] Describe how buffered streams (`BufferedOutputStream` and `BufferedInputStream`) can improve IO performance