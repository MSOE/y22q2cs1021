package wk8;

import java.util.function.Predicate;

public class IsReal implements Predicate<Complex> {
    @Override
    public boolean test(Complex complex) {
        return complex.getAngle()==0;
    }
}
