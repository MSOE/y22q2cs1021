package wk8;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Driver {
    public static void main(String[] args) {
        final Random generator = new Random();
        List<Complex> nums = new ArrayList<>();
        for(int i=0; i<50000; i++) {
            nums.add(new Complex(i, generator.nextInt(-8, 8)));
        }
        System.out.println(getReal(nums).size());
    }

    public static List<Complex> getReal(List<Complex> complexities) {
        IsReal isReal = new IsReal();
        return complexities.stream()
                .filter(isReal)
                .toList();
                //.collect(Collectors.toList());
    }
}
