package wk2;

import java.util.ArrayList;
import java.util.List;

public class WavFile extends DesktopItem {
    private List<Double> samples;
    private int numChannels;
    private long numFrames;
    private long sampleRate;

    public WavFile(String name) {
        super(name);
        samples = new ArrayList<>();
    }

    public List<Double> getSamples() {
        return samples;
    }

    public void setSamples(List<Double> samples) {
        this.samples = samples;
    }

    public int getNumChannels() {
        return numChannels;
    }

    public long getNumFrames() {
        return numFrames;
    }

    public long getSampleRate() {
        return sampleRate;
    }

    @Override
    public void open() {
        System.out.println("Opening the WavFile called " + getName());
    }

    public int size() {
        final int bytesPerDouble = 8;
        return super.size() + bytesPerDouble*samples.size();
    }
}
